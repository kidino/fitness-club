<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PenggunaController;

Route::get('/', [ HomeController::class, 'index' ]  );

Route::get('/about', [ HomeController::class, 'about' ]  );

Route::get('/contact', [ HomeController::class, 'contact' ]  );

Route::get('/selamat-datang', function(){
    echo "<h1>Selamat Datang</h1>";
});

Route::get('/selamat-datang/{name}', function($name){
    echo "<h1>Selamat Datang $name</h1>";
});

Route::get('/pengguna', [ PenggunaController::class, 'index' ]  );

Route::post('/pengguna', [ PenggunaController::class, 'store' ]  );

Route::get('/pengguna/create', [ PenggunaController::class, 'create' ]   );

Route::get('/pengguna/{user}', [ PenggunaController::class, 'details' ]  );
Route::put('/pengguna/{user}', [ PenggunaController::class, 'update' ]  );

