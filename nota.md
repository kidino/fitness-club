## Model 

* nama model mesti bermula dengan huruf besar
* nama model bersifat CamelCase 
* nama model bersifat singular 
* model merujuk kepada nama table plural 
* table biasanya mesti semuanya lowercase 
* table biasanya mesti ada column *id*

**Model** : User

**Table** : users 

**Model** : Book

**Table** : books 



