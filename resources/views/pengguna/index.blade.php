@extends('layouts.main')

@section('content')

    <h1>Senarai Pengguna</h1>

    <a href="/pengguna/create" class="btn btn-primary">
        Pengguna Baru
    </a>

    @if(session('success'))  
        <div class="alert alert-success mt-3">{{ session('success') }}</div>
    @endif 

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>NAMA</th>
                <th>EMAIL</th>
                <th>TARIKH DAFTAR</th>
                <th> </th>
            </tr>
        </thead>

        <tbody>
            @foreach( $users as $user )

            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->create_at }}</td>
                <td><a href="/pengguna/{{ $user->id }}" class="btn btn-primary">EDIT</a></td>
            </tr>

            @endforeach 

        </tbody>

    </table>




@endsection