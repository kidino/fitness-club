@extends('layouts.main')

@section('content') 

    <h2>Pengguna</h2>

    @if(session('success'))  
        <div class="alert alert-success mt-3">{{ session('success') }}</div>
    @endif     

    <form action="/pengguna/{{$user->id}}" method="post">
    @csrf 
    @method('PUT')

    <table class="table">
        <tbody>
        <tr>
                <th>Nama</th>
                <td>
                    <input type="text" name="name" 
                    value="{{ old('name', $user->name) }}"
                    class="form-control @error('name')) is-invalid @enderror">
                    @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror                     
                </td>
            </tr>
            <tr>
                <th>Email</th>
                <td>
                    <input type="email" name="email" 
                    value="{{ old('email', $user->email) }}"
                    class="form-control @error('email')) is-invalid @enderror">
                    @error('email')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror                      
                </td>
            </tr>
            <tr>
                <th>Password</th>
                <td>
                    <input type="password" name="password" 
                    value=""
                    class="form-control @error('password')) is-invalid @enderror">
                    @error('password')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror                      
                </td>
            </tr>

            <tr>
                <td><button class="btn btn-primary">Simpan</button></td>
                <td></td>
            </tr>
        </tbody>
    </table>

    </form>

@endsection 