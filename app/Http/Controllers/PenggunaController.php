<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PenggunaController extends Controller
{

    public function index() {
        $users = User::all();

        return view('pengguna.index', compact('users') );
    }

    public function create() {
        return view('pengguna.create');
    }

    public function store( Request $request ) {

        $validation_rules = [
            'name' => 'required|string|min:5|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:8'
        ];

        $custom_messages = [
            'name.required' => 'Sila isikan nama pengguna',
            'email.required' => 'Sila isikan email pengguna',
        ];

        $validated_data = $request->validate( $validation_rules, $custom_messages );

        User::create( $validated_data );

        return redirect('/pengguna')->with('success', 'New user has been added');
    }

    public function details( User $user ) {

        // $user = User::findOrFail($id);

        return view('pengguna', [ 'user' => $user ]);

    }

    public function update(Request $request, User $user) {
        $validation_rules = [
            'name' => 'required|string|min:5|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$user->id,
        ];

        if( strlen( $request->input('password')) > 0 ) {
            $validation_rules['password'] = 'min:8';
        }

        // dd($validation_rules);

        $validated_data = $request->validate( $validation_rules );

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if(strlen( $request->input('password')) > 0) {
            $user->password = Hash::make( $request->input('password') );
        }

        $user->save();

        return redirect('/pengguna/' . $user->id )->with('success', 'User has been updated');

    }
}
